const { getPaymentById, getSummary, getPaymentGroupedByPaymentGateway, sortPaymentsByAmount, createPayment } = require("../controllers/payment.controller")
const { getPayments, validate, create } = require("../services/payment.service");


jest.mock("../services/payment.service")

const dummyPayments = [{
    id: 'dummyId1',
    amount: 300,
    description: 'dummy description',
    payment_gateway: 'visa',
    pan: 'dummypan'
}, {
    id: 'dummyId2',
    amount: 200,
    description: 'dummy description',
    payment_gateway: 'naranja',
    pan: 'dummypan'
},
{
    id: 'dummyId3',
    amount: 1000,
    description: 'dummy description',
    payment_gateway: 'naranja',
    pan: 'dummypan'
}];
beforeAll(() => {
    getPayments.mockReturnValue(dummyPayments)
})
describe('When call getPaymentsById an existing id', () => {

    beforeEach(() => {
        getPayments.mockClear();
    })
    it('Should return the payment info if the id exists', () => {

        const resultExpected = {
            id: 'dummyId1',
            amount: 300,
            description: 'dummy description',
            payment_gateway: 'visa',
            pan: 'dummypan'
        }
        const payment = getPaymentById('dummyId1');
        expect(payment).toStrictEqual(resultExpected);
    })

    it('Should call getPayments once', () => {
        const payment = getPaymentById('dummyId1');

        expect(getPayments).toHaveBeenCalledTimes(1)
    })
    it('Should call getPayments once with no arguments', () => {
        const id = 'dummyId1';
        const payment = getPaymentById(id);
        expect(getPayments).toHaveBeenNthCalledWith(1)
    })
})
describe('When call getPaymentsById with a NON existing id', () => {
    it('Should return undefined if the id does not exist', () => {
        const payment = getPaymentById('notExistinId');
        expect(payment).toBeUndefined()
    })
})


describe('When call getSummary', () => {
    beforeEach(() => {
        getPayments.mockClear()
    })
    it('Should return the total as the sum of the payments', () => {
        const { total } = getSummary();
        expect(total).toBe(1500)
    })
    it('Should return the count as the total number of payments', () => {

        const { count } = getSummary();
        expect(count).toBe(3)
    })
    it('Should call getPayments once with no arguments', () => {
        const result = getSummary();
        expect(getPayments).toHaveBeenCalledTimes(1)
    })
})


describe('When call getPaymentGroupedByPaymentGateway', () => {
    beforeEach(() => {
        getPayments.mockClear()
    })
    it('Should return the payments grouped by their gateway', () => {
        const resultExpected = {
            'visa': [{
                id: 'dummyId1',
                amount: 300,
                description: 'dummy description',
                payment_gateway: 'visa',
                pan: 'dummypan'
            }],
            'naranja': [{
                id: 'dummyId2',
                amount: 200,
                description: 'dummy description',
                payment_gateway: 'naranja',
                pan: 'dummypan'
            },
            {
                id: 'dummyId3',
                amount: 1000,
                description: 'dummy description',
                payment_gateway: 'naranja',
                pan: 'dummypan'
            }]
        }
        const result = getPaymentGroupedByPaymentGateway();

        expect(result).toStrictEqual(resultExpected)

    })
    it('Should call getPayments once with no arguments', () => {
        const result = getSummary();
        expect(getPayments).toHaveBeenCalledTimes(1);
    });

})

describe('When call sortPaymentsByAmount with no arguments', () => {
    it('Should return array of payments order by lower amount first', () => {
        const resultExpected = [
            {
                id: 'dummyId2',
                amount: 200,
                description: 'dummy description',
                payment_gateway: 'naranja',
                pan: 'dummypan'
            },
            {
                id: 'dummyId1',
                amount: 300,
                description: 'dummy description',
                payment_gateway: 'visa',
                pan: 'dummypan'
            },
            {
                id: 'dummyId3',
                amount: 1000,
                description: 'dummy description',
                payment_gateway: 'naranja',
                pan: 'dummypan'
            }
        ];
        const result = sortPaymentsByAmount();
        expect(result).toStrictEqual(resultExpected);

    })
    it('Should return array with lenth equals to the total amount of payments', () => {
        const result = sortPaymentsByAmount();
        expect(result.length).toBe(3)
    })

    it('Should call getPayments once with no arguments', () => {
        const result = getSummary();
        expect(getPayments).toHaveBeenNthCalledWith(1);
    });
})

describe('When call sortPaymentsByAmount with false', () => {
    it('Should return array of payments order by higher amount first', () => {
        const resultExpected = [
            {
                id: 'dummyId3',
                amount: 1000,
                description: 'dummy description',
                payment_gateway: 'naranja',
                pan: 'dummypan'
            },
            {
                id: 'dummyId1',
                amount: 300,
                description: 'dummy description',
                payment_gateway: 'visa',
                pan: 'dummypan'
            },
            {
                id: 'dummyId2',
                amount: 200,
                description: 'dummy description',
                payment_gateway: 'naranja',
                pan: 'dummypan'
            },
        ];
        const result = sortPaymentsByAmount(false);
        expect(result).toStrictEqual(resultExpected);

    })
    it('Should return array with lenth equals to the total amount of payments', () => {
        const result = sortPaymentsByAmount();
        expect(result.length).toBe(3)
    })

    it('Should call getPayments once with no arguments', () => {
        const result = getSummary();
        expect(getPayments).toHaveBeenNthCalledWith(1);
    });
})

//Bonus
describe('When call createPayment ', () => {
    it('Should throw error and should not call create service', () => {
        validate.mockImplementation(() => {
            throw new Error('Invalid data')
        });
        create.mockReturnValue();
        const paymentCreated = createPayment({
            id: 'dummyId4',
            amount: 500,
            description: 'dummy description',
            payment_gateway: 'visa',
            pan: 'dummypan'
        },)
        expect(create).not.toHaveBeenCalled()
    })
})